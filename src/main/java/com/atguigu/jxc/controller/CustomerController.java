package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    /**
     * 客户列表分页
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @PostMapping("/list")
    @ResponseBody
    public Map<String, Object> getCustomerPage(Integer page,Integer rows,String customerName){
        Map<String, Object> map = customerService.getCustomerPage(page,rows,customerName);
        return map;
    }

    /**
     * 保存或修改客户
     * @param customer
     * @return
     */
    @PostMapping("/save")
    public Map<String, Object> saveCustomer(Customer customer){
        Map<String, Object> map =  customerService.save(customer);
        return map;
    }

    @PostMapping("/delete")
    public Map<String, Object> deleteCustomer (@RequestParam String ids){
        Map<String, Object> resultMap =  customerService.deleteSupplier(ids);
        return resultMap;
    }




}
