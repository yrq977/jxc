package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @PostMapping("/list")
    @ResponseBody
    public Map<String,Object> supplierList (Integer page,Integer rows,String supplierName){
        Map<String, Object> resultMap =  supplierService.supplierList(page,rows,supplierName);
        return resultMap;
    }

    /**
     * 保存或修改供应商
     * @param supplier
     * @return
     */
    @PostMapping("/save")
    public Map<String, Object> saveSupplier( Supplier supplier){
        Map<String, Object> resultMap =  supplierService.saveSupplier(supplier);
        return resultMap;
    }

    /**
     * 删除供应商
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public Map<String, Object> deleteSupplier(@RequestParam String ids){
        Map<String, Object> resultMap =  supplierService.deleteSupplier(ids);
        return resultMap;
    }


}
