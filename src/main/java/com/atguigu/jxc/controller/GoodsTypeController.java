package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.service.GoodsTypeService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;

/**
 * @description 商品类别控制器
 */
@RestController
@RequestMapping("/goodsType")
public class GoodsTypeController {

    @Autowired
    private GoodsTypeService goodsTypeService;

    /**
     * 查询所有商品类别
     * @return easyui要求的JSON格式字符串
     */
    @PostMapping("/loadGoodsType")
    @RequiresPermissions(value={"商品管理","进货入库","退货出库","销售出库","客户退货","当前库存查询","商品报损","商品报溢","商品采购统计"},logical = Logical.OR)
    public ArrayList<Object> loadGoodsType() {
        return goodsTypeService.loadGoodsType();
    }

    /**
     * 新增分类
     * @param goodsTypeName
     * @param pId
     * @return
     */
    @PostMapping("/save")
    @ResponseBody
    public Map<String,Object> savaGoodsType(String goodsTypeName,Integer pId){
        Map<String, Object> map =  goodsTypeService.savaGoodsType(goodsTypeName,pId);
        return map;
    }

    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    @PostMapping("/delete")
    public Map<String, Object> deleteGoodsType(@RequestParam Integer goodsTypeId){
        Map<String, Object> map = goodsTypeService.deleteGoodsType(goodsTypeId);
        return map;
    }

}
