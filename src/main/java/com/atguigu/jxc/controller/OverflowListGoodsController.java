package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {

    @Autowired
    private OverflowListGoodsService overflowListGoodsService;


    /**
     * 报溢单保存
     * @param session
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @PostMapping("/save")
    public Map<String, Object> saveOverFlowList(HttpSession session, OverflowList overflowList, String overflowListGoodsStr){
        Map<String, Object> map = overflowListGoodsService.saveOverFlowList(session,overflowList,overflowListGoodsStr);
        return map;
    }

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> getList(String  sTime, String  eTime){
        Map<String, Object> map = overflowListGoodsService.getList(sTime,eTime);
        return map;
    }

    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    @PostMapping("/goodsList")
    public Map<String, Object> getGoodsList(Integer overflowListId){
        Map<String, Object> map = overflowListGoodsService.getGoodsList(overflowListId);
        return map;
    }
}
