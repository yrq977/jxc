package com.atguigu.jxc.controller;

import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpSession;
import java.net.URI;
import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {

    @Autowired
    private DamageListGoodsService damageListGoodsService;


    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @PostMapping("/save")
    public Map<String, Object> saveDamageList(HttpSession session, DamageList damageList, String damageListGoodsStr){
        Map<String, Object> map = damageListGoodsService.saveDamageList(session,damageList,damageListGoodsStr);
        return map;
    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> getDamageListGoodsList(String sTime,String eTime){
        Map<String, Object> map = damageListGoodsService.getDamageListGoodsList(sTime,eTime);
        return map;
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @PostMapping("/goodsList")
    public Map<String, Object> getGoodsList (Integer damageListId){
        Map<String, Object> map = damageListGoodsService.getGoodsList(damageListId);
        return map;
    }

}
