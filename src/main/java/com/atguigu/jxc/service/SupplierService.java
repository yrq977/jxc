package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.List;
import java.util.Map;

public interface SupplierService {
    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    Map<String, Object> supplierList(Integer page, Integer rows, String supplierName);

    /**
     * 保存或修改供应商
     *
     * @param supplier
     * @return
     */
    Map<String, Object> saveSupplier(Supplier supplier);

    /**
     * 删除供应商
     * @param ids
     * @return
     */
    Map<String, Object> deleteSupplier(String ids);
}
