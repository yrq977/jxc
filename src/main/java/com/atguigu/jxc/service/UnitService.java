package com.atguigu.jxc.service;

import java.util.Map;

public interface UnitService {
    /**
     * 获取所有商品单位
     * @return
     */
    Map<String, Object> getUnitList();
}
