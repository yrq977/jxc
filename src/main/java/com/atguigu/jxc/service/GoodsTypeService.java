package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;

import java.util.ArrayList;
import java.util.Map;

/**
 * @description
 */
public interface GoodsTypeService {
    ArrayList<Object> loadGoodsType();

    /**
     * 新增分类
     * @param goodsTypeName
     * @param pId
     * @return
     */
    Map<String, Object> savaGoodsType(String goodsTypeName, Integer pId);

    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    Map<String, Object> deleteGoodsType(Integer goodsTypeId);
}
