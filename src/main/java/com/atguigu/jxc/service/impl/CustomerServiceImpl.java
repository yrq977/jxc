package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    /**
     * 客户列表分页
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @Override
    public Map<String, Object> getCustomerPage(Integer page, Integer rows, String customerName) {
        HashMap<String, Object> resultMap = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Customer> customerList =  customerDao.getCustomerPage(offSet,rows,customerName);
        Integer total = customerDao.getCount();
        resultMap.put("total",total);
        resultMap.put("rows",customerList);
        return resultMap;
    }

    /**
     * 保存或修改客户
     * @param customer
     * @return
     */
    @Override
    public Map<String, Object> save(Customer customer) {
        if(customer.getCustomerId() == null){
            //保存
            customerDao.save(customer);
        }else {
            //修改
            customerDao.update(customer);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("code",100);
        map.put("msg","请求成功");
        map.put("info",null);
        return map;
    }

    @Override
    public Map<String, Object> deleteSupplier(String ids) {
        if(ids.contains(",")){
            String[] split = ids.split(",");
            ArrayList<Integer> integers = new ArrayList<>();
            for (String s : split) {
                integers.add(Integer.parseInt(s));
            }
            for (Integer integer : integers) {
                System.out.println(integer);
                customerDao.deleteSupplier(integer);
            }
        }else {
            customerDao.deleteSupplier(Integer.parseInt(ids));
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("code",100);
        map.put("msg","请求成功");
        map.put("info",null);
        return map;
    }
}
