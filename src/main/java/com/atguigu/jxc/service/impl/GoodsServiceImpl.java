package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String, Object> getGoodsPage(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String,Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goods = goodsDao.getGoodsList(offSet,rows,codeOrName,goodsTypeId);
        for (Goods good : goods) {
            //根据类型id获取类型名字
            good.setGoodsTypeName(goodsDao.getTypeName(good.getGoodsTypeId()));
            //根据商品库存id获取销售总量
            good.setSaleTotal(goodsDao.getsaletotal(good.getGoodsId()));
        }
        Integer count = goodsDao.getGoodsCount();
        map.put("total",count);
        map.put("rows",goods);
        return map;
    }

    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String, Object> getGoodsListPage(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Map<String,Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsList =  goodsDao.getGoodsListPage(offSet,rows,goodsName,goodsTypeId);
        for (Goods goods : goodsList) {
            //根据类型id获取类型名字
            goods.setGoodsTypeName(goodsDao.getTypeName(goods.getGoodsTypeId()));
        }
        Integer total = goodsDao.getGoodsCount();
        map.put("total",total);
        map.put("rows",goodsList);
        return map;
    }

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
    @Override
    public Map<String, Object> save(Goods goods) {
        if(goods.getGoodsId()==null){
            goodsDao.save(goods);
        }else {
            goodsDao.update(goods);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("code",100);
        map.put("msg","请求成功");
        map.put("info",null);
        return map;
    }

    /**
     * 删除商品信息
     * @param goodsId
     * @return
     */
    @Override
    public Map<String, Object> delete(Integer goodsId) {
        HashMap<String, Object> map = new HashMap<>();
        //根据id获取 goods
        Goods goods =  goodsDao.getGoodsById(goodsId);
        //判断 goods状态 如果为0 删除
        if(goods.getState() == 0){
            goodsDao.delete(goodsId);
            map.put("code",100);
            map.put("msg","请求成功");
            map.put("info",null);
        }else {
            new RuntimeException("无法删除");
        }
        return map;
    }

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        HashMap<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsList =  goodsDao.getNoInventoryQuantity(offSet,rows,nameOrCode);
        for (Goods goods : goodsList) {
            //根据类型id获取类型名字
            goods.setGoodsTypeName(goodsDao.getTypeName(goods.getGoodsTypeId()));
        }
        //获取无库存商品总量
        Integer total =  goodsDao.getNoInventoryQuantityTotal();
        map.put("total",total);
        map.put("rows",goodsList);
        return map;
    }

    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        HashMap<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsList =  goodsDao.getHasInventoryQuantity(offSet,rows,nameOrCode);
        for (Goods goods : goodsList) {
            //根据类型id获取类型名字
            goods.setGoodsTypeName(goodsDao.getTypeName(goods.getGoodsTypeId()));
        }
        //获取无库存商品总量
        Integer total =  goodsDao.getHasInventoryQuantityTotal();
        map.put("total",total);
        map.put("rows",goodsList);
        return map;
    }

    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    @Override
    public Map<String, Object> saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice) {
        goodsDao.saveStock(goodsId,inventoryQuantity,purchasingPrice);
        HashMap<String, Object> map = new HashMap<>();
        map.put("code",100);
        map.put("msg","请求成功");
        map.put("info",null);
        return map;
    }

    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     */
    @Override
    public Map<String, Object> deleteStock(Integer goodsId) {
        Goods goods = goodsDao.getGoodsById(goodsId);
        if(goods.getState()==0){
            goodsDao.delete(goodsId);
            HashMap<String, Object> map = new HashMap<>();
            map.put("code",100);
            map.put("msg","请求成功");
            map.put("info",null);
            return map;
        }else {
            new RuntimeException("删除失败");
        }
        return null;
    }

    @Override
    public Map<String, Object> listAlarm() {
        HashMap<String, Object> map = new HashMap<>();
        List<Goods> goodsList= goodsDao.findAll();
        ArrayList<Goods> goodsArrayList = new ArrayList<>();
        for (Goods goods : goodsList) {
            if(goods.getInventoryQuantity()<goods.getMinNum()){
                goods.setGoodsTypeName(goodsDao.getTypeName(goods.getGoodsTypeId()));
                goodsArrayList.add(goods);
            }
        }
        map.put("rows",goodsArrayList);
        return map;
    }


}
