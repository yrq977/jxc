package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;

    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @Override
    public Map<String, Object> supplierList(Integer page, Integer rows, String supplierName) {
        HashMap<String, Object> resultMap = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Supplier> suppliers =  supplierDao.supplierList(offSet,rows,supplierName);
        Integer total = supplierDao.getSupplierTotal();
        resultMap.put("total",total);
        resultMap.put("rows",suppliers);
        return resultMap;
    }

    /**
     * 保存或修改供应商
     *
     * @param supplier
     * @return
     */
    @Override
    public Map<String, Object> saveSupplier(Supplier supplier) {
        //如果id为空进行保存
        if(supplier.getSupplierId()==null){
            supplierDao.saveSupplier(supplier);
        }else {
            //进行修改
            supplierDao.update(supplier);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("code",100);
        map.put("msg","请求成功");
        return map;
    }

    /**
     * 删除供应商
     * @param ids
     * @return
     */
    @Override
    public Map<String, Object> deleteSupplier(String ids) {
        if(ids.contains(",")){
            String[] split = ids.split(",");
            ArrayList<Integer> integers = new ArrayList<>();
            for (String s : split) {
                int i = 0;
                i = Integer.parseInt(s);
                integers.add(i);
            }
            for (Integer integer : integers) {
                System.out.println(integer);
                supplierDao.deleteSupplier(integer);
            }
        }else {
            supplierDao.deleteSupplier(Integer.parseInt(ids));
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("code",100);
        map.put("msg","请求成功");
        map.put("info",null);
        return map;
    }
}
