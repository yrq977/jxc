package com.atguigu.jxc.service.impl;


import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {

    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> getList(String sTime, String eTime) {
        HashMap<String, Object> map = new HashMap<>();
        List<OverflowList> overflowLists = overflowListGoodsDao.getList();
        ArrayList<OverflowList> overflowListArrayList = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date startTime = simpleDateFormat.parse(sTime);
            Date endTime = simpleDateFormat.parse(eTime);
            for (OverflowList overflowList  : overflowLists) {
                Date damageTime = simpleDateFormat.parse(overflowList.getOverflowDate());
                if(damageTime.after(startTime) && damageTime.before(endTime)){
                    overflowList.setTrueName(overflowListGoodsDao.getTrueName(overflowList.getUserId()));
                    overflowListArrayList.add(overflowList);
                }
            }
            map.put("rows",overflowListArrayList);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        return map;
    }

    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    @Override
    public Map<String, Object> getGoodsList(Integer overflowListId) {
        HashMap<String, Object> map = new HashMap<>();
        List<OverflowListGoods> overflowListGoodsList =  overflowListGoodsDao.getGoodsList(overflowListId);
        map.put("rows",overflowListGoodsList);
        return map;
    }

    /**
     * 报溢单保存
     * @param session
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @Override
    public Map<String, Object> saveOverFlowList(HttpSession session, OverflowList overflowList, String overflowListGoodsStr) {
        HashMap<String, Object> map = new HashMap<>();
        User user =(User) session.getAttribute("currentUser");
        overflowList.setUserId(user.getUserId());
        overflowListGoodsDao.saveOverFlowList(overflowList);
        OverflowListGoods overflowListGoods = cast(overflowListGoodsStr);
        overflowListGoodsDao.saveOverFlowListGoods(overflowListGoods);
        map.put("code",100);
        map.put("msg","请求成功");
        map.put("info",null);
        return map;
    }

    public OverflowListGoods cast(String overflowListGoodsStr){
        Gson gson = new Gson();
        JsonArray jsonArray = gson.fromJson(overflowListGoodsStr, JsonArray.class);
        OverflowListGoods overflowListGoods = new OverflowListGoods();
        for (JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            overflowListGoods = gson.fromJson(jsonObject, OverflowListGoods.class);
        }
        return overflowListGoods;
    }
}
