package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import jdk.nashorn.internal.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {

    @Autowired
    private DamageListGoodsDao damageListGoodsDao;

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> getDamageListGoodsList(String sTime, String eTime) {
        HashMap<String, Object> map = new HashMap<>();
        List<DamageList> damageLists = damageListGoodsDao.getDamageListGoodsList();
        ArrayList<DamageList> damageListArrayList = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date startTime = simpleDateFormat.parse(sTime);
            Date endTime = simpleDateFormat.parse(eTime);
            for (DamageList damageList : damageLists) {
                Date damageTime = simpleDateFormat.parse(damageList.getDamageDate());
                if(damageTime.after(startTime) && damageTime.before(endTime)){
                    damageList.setTrueName(damageListGoodsDao.getTrueName(damageList.getUserId()));
                    damageListArrayList.add(damageList);
                }
            }
            map.put("rows",damageListArrayList);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        return map;
    }

    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @Override
    public Map<String, Object> saveDamageList(HttpSession session, DamageList damageList, String damageListGoodsStr) {
        HashMap<String, Object> map = new HashMap<>();
        User user =(User) session.getAttribute("currentUser");
        damageList.setUserId(user.getUserId());
        damageListGoodsDao.saveDamageList(damageList);
        JSONArray jsonArray = JSON.parseArray(damageListGoodsStr);
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            DamageListGoods damageListGoods = new DamageListGoods();
            damageListGoods.setGoodsId(jsonObject.getInteger("goodsId"));
            damageListGoods.setGoodsTypeId(jsonObject.getInteger("goodsTypeId"));
            damageListGoods.setGoodsCode(jsonObject.getString("goodsCode"));
            damageListGoods.setGoodsName(jsonObject.getString("goodsName"));
            damageListGoods.setGoodsModel(jsonObject.getString("goodsModel"));
            damageListGoods.setGoodsUnit(jsonObject.getString("goodsUnit"));
            damageListGoods.setPrice(jsonObject.getDouble("price"));
            damageListGoods.setGoodsNum(jsonObject.getInteger("goodsNum"));
            damageListGoods.setTotal(jsonObject.getDouble("total"));
            damageListGoods.setDamageListId(damageListGoodsDao.getDamageListId(damageList.getDamageNumber()));
            damageListGoodsDao.saveDamageListGoods(damageListGoods);
        }
        map.put("code",100);
        map.put("msg","请求成功");
        map.put("info",null);
        return map;
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @Override
    public Map<String, Object> getGoodsList(Integer damageListId) {
        HashMap<String, Object> map = new HashMap<>();
        List<DamageListGoods> damageListGoodsList = damageListGoodsDao.getGoodsList(damageListId);
        map.put("rows",damageListGoodsList);
        return map;
    }




}
