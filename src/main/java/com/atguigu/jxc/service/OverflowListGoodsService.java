package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface OverflowListGoodsService {
    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> getList(String sTime, String eTime);

    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    Map<String, Object> getGoodsList(Integer overflowListId);

    /**
     * 报溢单保存
     * @param session
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    Map<String, Object> saveOverFlowList(HttpSession session, OverflowList overflowList, String overflowListGoodsStr);
}
