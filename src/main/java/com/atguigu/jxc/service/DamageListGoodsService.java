package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface DamageListGoodsService {

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> getDamageListGoodsList(String sTime, String eTime);

    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    Map<String, Object> saveDamageList(HttpSession session, DamageList damageList, String damageListGoodsStr);

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    Map<String, Object> getGoodsList(Integer damageListId);
}
