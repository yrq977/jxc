package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

public interface CustomerService {
    /**
     * 客户列表分页
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    Map<String, Object> getCustomerPage(Integer page, Integer rows, String customerName);

    /**
     * 保存或修改客户
     * @param customer
     * @return
     */
    Map<String, Object> save(Customer customer);

    Map<String, Object> deleteSupplier(String ids);
}
