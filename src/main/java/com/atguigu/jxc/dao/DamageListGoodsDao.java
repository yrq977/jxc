package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListGoodsDao {
    List<DamageList> getDamageListGoodsList();

    String getTrueName(@Param("userId") Integer userId);

    void saveDamageList(@Param("damageList") DamageList damageList);

    void saveDamageListGoods(@Param("damageListGoods") DamageListGoods damageListGoods);

    List<DamageListGoods> getGoodsList(@Param("damageListId") Integer damageListId);

    Integer getDamageListId(@Param("damageNumber") String damageNumber);
}
