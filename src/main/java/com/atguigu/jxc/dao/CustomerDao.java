package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {
    /**
     * 客户分页列表
     * @param offSet
     * @param rows
     * @param customerName
     * @return
     */
    List<Customer> getCustomerPage(@Param("offSet") int offSet,@Param("rows") Integer rows,@Param("customerName") String customerName);

    Integer getCount();

    void save(@Param("customer") Customer customer);

    void update(@Param("customer") Customer customer);

    void deleteSupplier(@Param("integer") Integer integer);
}
