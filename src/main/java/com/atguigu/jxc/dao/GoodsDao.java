package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    /**
     * 分页查询商品库存信息
     * @param offSet 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    List<Goods> getGoodsList(@Param("offset") Integer offSet, @Param("rows") Integer rows, @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);


    Integer getGoodsCount();

    String getTypeName(@Param("goodsTypeId") Integer goodsTypeId);

    Integer getsaletotal(@Param("goodsId") Integer goodsId);

    List<Goods> getGoodsListPage(@Param("offset") Integer offSet, @Param("rows") Integer rows, @Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    void save(@Param("goods") Goods goods);

    void update(@Param("goods")Goods goods);

    Goods getGoodsById(@Param("goodsId") Integer goodsId);

    void delete(@Param("goodsId")Integer goodsId);

    List<Goods> getNoInventoryQuantity(@Param("offSet") Integer offSet,@Param("rows") Integer rows,@Param("nameOrCode") String nameOrCode);

    Integer getNoInventoryQuantityTotal();

    List<Goods> getHasInventoryQuantity(@Param("offSet") Integer offSet,@Param("rows") Integer rows,@Param("nameOrCode") String nameOrCode);

    Integer getHasInventoryQuantityTotal();

    void saveStock(@Param("goodsId") Integer goodsId,@Param("inventoryQuantity") Integer inventoryQuantity,@Param("purchasingPrice") Double purchasingPrice);


    List<Goods> findAll();
}
