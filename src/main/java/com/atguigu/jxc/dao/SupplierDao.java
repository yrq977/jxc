package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierDao {

    List<Supplier> supplierList(@Param("offSet") Integer offSet,@Param("rows") Integer rows,@Param("supplierName") String supplierName);

    Integer getSupplierTotal();

    void saveSupplier(@Param("supplier") Supplier supplier);

    void update(@Param("supplier") Supplier supplier);

    void deleteSupplier(@Param("integers") Integer integers);
}
