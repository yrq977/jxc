package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverflowListGoodsDao {
    List<OverflowList> getList();

    String getTrueName(@Param("userId") Integer userId);

    List<OverflowListGoods> getGoodsList(@Param("overflowListId") Integer overflowListId);

    void saveOverFlowList(@Param("overflowList") OverflowList overflowList);

    void saveOverFlowListGoods(@Param("overflowListGoods") OverflowListGoods overflowListGoods);
}
